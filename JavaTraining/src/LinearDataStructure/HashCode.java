package LinearDataStructure;

import java.util.Scanner;



public class HashCode {
	
	public static HashNode[] buckets;
	public static int noBuckets;//capacity
	public int size;//no of key=value pair in hash table
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashTable h=new HashTable(10);
		Scanner sc=new Scanner(System.in);
		int q=1;
		System.out.println("HashTable Working");
		while(q!=0) {
			System.out.println("1.Insert  2.Size  3.Delete  4.Contains  5.Get value by key  6.Display  7.Iterate");
		
			int op=sc.nextInt();
			switch(op) {
			case 1:
				System.out.println("Enter the element to insert=");
				int key=sc.nextInt();
				String value=sc.next();
				h.put(key, value);
				break;
			case 2:
				System.out.println("Size of HAshTable=");
				System.out.println(h.size());
				break;
			case 3:
				System.out.println("Enter the key for which you want to delete=");
				int Rkey=sc.nextInt();
				h.remove(Rkey);
				h.print();
				break;
			case 4:
				System.out.println("Enter the key=");
				int k=sc.nextInt();
				System.out.println(h.ContainKey(k));
				break;
			case 5:
				System.out.println("Enter the key for which you want value=");
				int Gkey=sc.nextInt();
				System.out.println(h.get(Gkey));
				break;
			case 6:
				h.print();
				break;
			case 7:
				h.iterator();
				break;
		
		
			}
		}
	}
}
