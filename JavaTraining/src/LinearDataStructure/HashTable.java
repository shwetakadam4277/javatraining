package LinearDataStructure;
import java.util.*;
 
public class HashTable {
	
public static HashNode[] buckets;
public static int noBuckets;//capacity
public int size;//no of key=value pair in hash table

public HashTable() {
	this(10);//default capacity
	
}

public HashTable(int capacity) {
	this.noBuckets=capacity;
	this.buckets=new HashNode[noBuckets];
	this.size=0;
}




public int size() {
	return size;
}

public boolean isEmpty() {
	return size==0;
}

public void put(Integer key,String value) {
	if(key==null ||value==null) {
		throw new IllegalArgumentException("Key/Value is null");
	}
	int bucketIndx=getBucketIndx(key);
	HashNode head=buckets[bucketIndx];
	
	while(head!=null) {
		if(head.key.equals(key)) {
			head.value=value;
			return;
		}
		head=head.next;
	}
	size++;
	head=buckets[bucketIndx];
	HashNode node=new HashNode(key,value); //key,value->null
	node.next=head;
	buckets[bucketIndx]=node;
}


private int getBucketIndx(Integer key) {
	// TODO Auto-generated method stub
	return key%noBuckets;  //or buck.length;
}

public String get(Integer key) {
	if(key==null ) {
		throw new IllegalArgumentException("Keyis null");
	}
	
	int bucketIndx=getBucketIndx(key);
	HashNode head=buckets[bucketIndx];
	
		while(head!=null) {
			if(head.key.equals(key)) {
				return head.value;
				
			}
			head=head.next;
		}
	
	
	return null;
}


public String remove(Integer key) {
	if(key==null ) {
		throw new IllegalArgumentException("Keyis null");
	}
	int bucketIndx=getBucketIndx(key);
	HashNode head=buckets[bucketIndx];
	HashNode prev=null;
	
	while(head!=null) {
		if(head.key.equals(key)) {
			break;
			
		}
		prev=head;
		head=head.next;
	}
	if(head==null) {
		return null;
	}
	size--;
	if(prev!=null) {
		prev.next=head.next;
	}else {
		buckets[bucketIndx]=head.next;
	}
	
	return head.value;
}
public void print() {
	for(int i=0;i<buckets.length;i++) {
		System.out.print(i);
		HashNode ls=buckets[i];
		while(ls!=null) {
			System.out.print("{"+ls.key+","+ls.value+"}");
			ls=ls.next;
		}
		System.out.println();
	}
	
}

public void iterator() {
	for(int i=0;i<buckets.length;i++) {
		System.out.print(i+"=");
		HashNode ls=buckets[i];
		while(ls!=null) {
			System.out.print("{"+ls.key+","+ls.value+"}");
			ls=ls.next;
		}
		System.out.println();
	}
	
}

public boolean ContainKey(int key) {
	int buck=getBucketIndx(key);
	HashNode ls=buckets[buck];
	while(ls!=null) {
		if(ls.key.equals(key)) {
		return true;
		}
		ls=ls.next;
	}
	
	return false;
}



//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		HashTable h=new HashTable(10);
//		Scanner sc=new Scanner(System.in);
//		int q=1;
//		System.out.println("HashTable Working");
//		while(q!=0) {
//			System.out.println("1.Insert  2.Size  3.Delete  4.Contains  5.Get value by key  6.Display  7.Iterate");
//		
//			int op=sc.nextInt();
//			switch(op) {
//			case 1:
//				System.out.println("Enter the element to insert=");
//				int key=sc.nextInt();
//				String value=sc.next();
//				h.put(key, value);
//				break;
//			case 2:
//				System.out.println("Size of HAshTable=");
//				System.out.println(h.size());
//				break;
//			case 3:
//				System.out.println("Enter the key for which you want to delete=");
//				int Rkey=sc.nextInt();
//				h.remove(Rkey);
//				h.print();
//				break;
//			case 4:
//				System.out.println("Enter the key=");
//				int k=sc.nextInt();
//				System.out.println(h.ContainKey(k));
//				break;
//			case 5:
//				System.out.println("Enter the key for which you want value=");
//				int Gkey=sc.nextInt();
//				System.out.println(h.get(Gkey));
//				break;
//			case 6:
//				h.print();
//				break;
//			case 7:
//				for(int i=0;i<buckets.length;i++) {
//					System.out.print(i+"=");
//					HashNode ls=buckets[i];
//					while(ls!=null) {
//						System.out.print("{"+ls.key+","+ls.value+"}");
//						ls=ls.next;
//					}
//					System.out.println();
//				}
//				break;
//		
//		
//		}
//		h.put(105, "tom");
//		h.put(106, "jerry");
//		h.put(107, "shweta");
//		h.put(108, "bunny");
////		h.print();
////		String str=h.get(105);
//		
//				
//		System.out.println(h.size());
//		
//		System.out.println(h.get(107));
//		
//		System.out.println(h.remove(108));
//		h.print();
		
		
		
		
		
		
		

//	}

	}
//}
