package LinearDataStructure;
import java.util.Scanner;
//import LinearDataStructure.LLMain.Node;
import java.util.Iterator;
public class LLMain{
	static class Node
	{
	 int data;
	 Node next;
	
	 public Node(int data)
	 {
	     this.data = data;
	     this.next = next;
	 }
	 
	 
	}
	static Node head;
	public void add(int data) {
	 	Node toinsert=new Node(data);
	 	if(head==null) {
	 		head=toinsert;
	 		return;
	 	}
	 	Node temp=head;
	 	while(temp.next!=null) {
	 		temp=temp.next;
	 	}
	 	temp.next=toinsert;
	 }
	public void printlist(Node temp){
	      temp=head;
	     while(temp!=null){
	         System.out.println(temp.data);
	         temp=temp.next;
	     }
	 }
	int getcount(){
		 if(head==null){
		     return 0;
		 }
		 Node temp=head;
		 int c=0;
		 while(temp!=null){
		     c++;
		     temp=temp.next;
		 }
		 return c;
		}
	public static Node reverse(Node node) {
		Node previous=null;
		Node current=node;
		Node next=null;
		while(current!=null) {
			next=current.next;
			current.next=previous;
			previous=current;
			current=next;
		}
		node=previous;
		return node;
		
	}
	void delete(int position) {
		if(head==null) {
			return;
		}
		Node temp=head;
		if(position==0) {
			head=temp.next;
			return;
		}
		for(int i=0;temp!=null &&i<position-1;i++) {
			temp=temp.next;
		}
		if(temp==null || temp.next==null) {
			return;
		}
		Node next=temp.next.next;
		temp.next=next;
	}
	public void sort() {
		Node current=head;
		Node index=null;
		int temp;
		if(head==null) {
			return;
		}
		else {
			while(current!=null) {
				index=current.next;
				while(index!=null) {
					if(current.data>index.data) {
						temp=current.data;
						current.data=index.data;
						index.data=temp;
					}
					index=index.next;
				}
				current=current.next;
				
			}
		}
		
	}
	public void middle() {
		if(head!=null) {
			int length=getcount();
			Node temp=head;
			int midLen=length/2;
			while(midLen!=0) {
				temp=temp.next;
				midLen--;
			}
			System.out.println("Middle Element is["+temp.data+"]");
		}
	}
	 static Node InsertPos(Node headNode,int position,int data) {
		Node head=headNode;
		if(position<1) {
			System.out.println("Invalid Position");
		}
		if(position==1) {
			Node newNode=new Node(data);
			newNode.next=headNode;
			head=newNode;
		}else {
			while(position--!=0) {
				if(position==1) {
					Node newNode=getNode(data);
					newNode.next=headNode.next;
					headNode.next=newNode;
					break;
				}
				headNode=headNode.next;
			}
			if(position!=1) {
				System.out.println("Position is not present");
			}
		}
		return head;
		
	}
     static Node getNode(int data) {
		
		return new Node(data);
	}
     void deletePos(int position) {
    	 if(head==null) {
    		 return;
    	 }
    	 Node temp=head;
    	 
    	 if(position==1) {
    		 head=temp.next;
    		 return;
    	 }
    	 for(int i=0;temp!=null&& i<position-1;i++) {
    		 temp=temp.next;
    	 }
    	 if(temp==null || temp.next==null) {
    		 return;
    	 }
    	 Node next=temp.next.next;
    	 
    	 temp.next=next;
     }
	public static void main(String[] args)
    {
    	LLMain h=new LLMain();
    	Scanner sc=new Scanner(System.in);
    	int q=1;
		System.out.println("LL Working");
		while(q!=0) {
			
			System.out.println("1.Insert  2.Display  3.Size 4.Reverse  5.Delete  6.Sort  7.Center  8.Insert at Position  9.Delete at Position  10.Iterator");
			int op=sc.nextInt();
			switch(op) {
			case 1:
				int ele=sc.nextInt();
				h.add(ele);
				break;
			case 2:
				h.printlist(head);
				break;
			case 3:
				int y=h.getcount();
				System.out.println("Size of LinkedList is="+y);
       			break;
			case 4:
				head=h.reverse(head);
				System.out.println("reversed LL is=");
				h.printlist(head);
				break;
			case 5:
				System.out.println("Enter the position to be deleted=");
				int pos=sc.nextInt();
				h.delete(pos);
				break;
			case 6:
				System.out.println("Sorted Linked list is=");
				h.sort();
				h.printlist(head);
				break;
			case 7:
				h.middle();
				break;
			case 8:
				System.out.println("insert the data to add=");
				int data=sc.nextInt();
				System.out.println("Insert the position to add data=");
				int posi=sc.nextInt();
				head=h.InsertPos(head, posi, data);
				System.out.println("Linked List after insertion=");
				h.printlist(head);
				break;
			case 9:
				System.out.println("Enter the position to delete node=");
				int poss=sc.nextInt();
				h.deletePos(poss);
				System.out.println("After Deletion at position=");
				h.printlist(head);
				break;
			case 10:
				Iterator t=LLMain.iterator();
				
//				while(t.hasNext()) {
//					System.out.println(t.next());
//				}
		}
    }
   }
	static Iterator iterator() {
		 Node temp=head;
		 System.out.println("Iterator Elements=");
	     while(temp!=null){
	         System.out.println(temp.data);
	         temp=temp.next;
	     }
		return (Iterator) temp;
	}
  }