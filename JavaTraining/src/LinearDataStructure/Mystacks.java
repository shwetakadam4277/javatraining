package LinearDataStructure;

import java.util.Scanner;

public class Mystacks {
	final int max=10;
	int a[]=new int[max];
	int top=-1;
	
//	Push
	void push(int ele) {
		if(top>=max-1) {
			System.out.println("stack overflow");
		}
		else {
			top++;
			a[top]=ele;
		}
	}
//	Pop
	int pop() {
		int z=0;
		if(top==-1) {
			System.out.println("Stack underflow");
		}else {
			z=a[top];
			top--;
		}
		return z;
	}
//	Display
	 void display() {
		// TODO Auto-generated method stub
		if(top==-1) {
			System.out.println("Stack empty");
			
		}else {
			for(int i=top;i>=0;i--) {
				System.out.println(a[i]+" ");
			}
		}
	}
//	 Reverse
	 void reverse() {
			if(top>=0) {
					for(int i=0;i<=top;i++) {
					System.out.println(a[i]);
				}
			}else {
				System.out.println("Stack is empty.No elements to reverse");
			}
	 }
//	 Size
	 void size() {
//			int count;	
//				count=a.length;
//			System.out.println("Size of stack is:"+count);
		 int count=top+1;
		 System.out.println("Size of stack is:"+count);
		
		}
//	 Peek
	 int peek() {
		 if(top==-1) {
			 System.out.println("Stack Underflow ");
		 }else {
			 int ele=a[top];
			 System.out.println(ele+" ");
		 }
		return a[top];
	 }
//	 Search
	 void Search(int num) {
		 
		
		 int pos=-1;
		 if(top==-1) {
			 System.out.println("Sttack underflow");
		 }else {
			 for(int i=0;i<=top;i++) {
				 if(num==a[i]) {
					 pos=i+1;
					 break;
				 }
			 }
			 if(pos==-1) {
				 System.out.println("Element not found");
			 }else {
				 System.out.println("found at ="+pos);
			 }
		 }
	 }
//	 Center
	 void Center() {
		int length=top+1;
		System.out.println("Middle element is "+a[length/2]);
	 }
//	 sort
	 void sort() {
		 int temp;
			for(int i=0;i<top;i++) {
				for(int j=i;j<top;j++) {
					if(a[i]>a[j]) {
						temp=a[i];
						a[i]=a[j];
						a[j]=temp;
					}
				}
			}
			for(int n:a) {
				System.out.println(n);
			}
	 }
	 
//	Iterate
	 public void iterate() {
		 System.out.println("Iterated elements are=");
		 for(int i=0;i<top;i++) {
			 System.out.println(a[i]);
		 }
		 
	 }
	 
	

}
