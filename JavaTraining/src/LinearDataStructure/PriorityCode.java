package LinearDataStructure;

import java.util.Scanner;

public class PriorityCode {
//Driver code
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		PriorityQueue p=new PriorityQueue();
		int q=1;
		System.out.println("Priority Queue Working");
		while(q!=0) {
			System.out.println("1.Insert  2.Reverse  3.Center  4.Peek(high priority)  5.Delete  6.Size   7.Display  8.Contains  9.Iterate");
			int op=sc.nextInt();
			switch(op) {
			case 1:
				System.out.println();
				int n=sc.nextInt();
				p.insert(n);
				p.display();
				break;
			case 2:
				System.out.println();
				p.reverse();
				break;
			case 3:
				System.out.println();
				
				p.center();
				break;
			case 4:
				System.out.println();
				//Node with maximum priority
				System.out.print("Node with Peek priority : " +
									p.extractMax() + "\n");
				break;
			case 5:
				System.out.println();
				System.out.println("Enter the position to remove=");
				int pos=sc.nextInt();
				p.remove(pos);
				System.out.print("Priority queue after " +
								"removing the element : ");
				p.display();

				System.out.println();
				break;
			case 6:
				System.out.println("Size of queue="+p.size());
//				PriorityQueue.size();
				break;
			case 7:
			
				p.display();
				break;
			case 8:
				System.out.println("Enter the element to search=");
				int key=sc.nextInt();
				p.Search(key);
				break;
			case 9:
				p.iterate();
				break;
				
			}
		}
	}

}
