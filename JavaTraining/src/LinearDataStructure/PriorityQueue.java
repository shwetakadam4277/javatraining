package LinearDataStructure;
import java.util.*;
class PriorityQueue{

static int []H = new int[50];
static int size = -1;

//Function to return the index of the parent node of a given node
	static int parent(int i){
	return (i - 1) / 2;
	}

//Function to return the index of the left child of the given node
	static int leftChild(int i){
	return ((2 * i) + 1);
	}

//Function to return the index of the right child of the given node
	static int rightChild(int i){
	 return ((2 * i) + 2);
	}

//Function to shift up

	static void shiftUp(int i){
		while (i > 0 && H[parent(i)] < H[i])
		{
			// Swap parent and current node
			swap(parent(i), i);
		
			// Update i to parent of i
			i = parent(i);
		}
	}

//Function to shift down the node

	static void shiftDown(int i){
	int maxIndex = i;
	
	//Left Child
	int l = leftChild(i);
	
	if (l <= size && H[l] > H[maxIndex])
	{
		maxIndex = l;
	}

//Right Child
	int r = rightChild(i);

	if (r <= size && H[r] > H[maxIndex]){
		maxIndex = r;
	}

//If i not same as maxIndex
	if (i != maxIndex){
		swap(i, maxIndex);
		shiftDown(maxIndex);
	}
	}

//Function to insert a new element 
	static void insert(int p){
		size = size + 1;
		H[size] = p;
		
		//Shift Up to maintain
		//heap property
		shiftUp(size);
	}

//Function to extract the element with maximum priority
	static int extractMax(){
		int result = H[0];
		
		//Replace the value
		//at the root with
		//the last leaf
		H[0] = H[size];
		size = size - 1;
		
		//Shift down the replaced
		//element to maintain the
		//heap property
		shiftDown(0);
		return result;
	}

//Function to change the priority
//of an element
static void changePriority(int i,int p)
{
	int oldp = H[i];
	H[i] = p;
	
	if (p > oldp){
		shiftUp(i);
	}
	else{
		shiftDown(i);
	}
}

//Function to get value of the current maximum element
	static int getMax(){
		return H[0];
	}

//Function to remove the element located at given index
	static void remove(int i){
	H[i] = getMax() + 1;
	
	//Shift the node to the root of the heap
	shiftUp(i);
	
	//Extract the node
	extractMax();
	}

	static void swap(int i, int j)
	{
		int temp= H[i];
		H[i] = H[j];
		H[j] = temp;
	}
	static void center() {
		int middle=size/2;
		System.out.println("Center of queue is ="+H[middle]);
	}
	public static void iterator() {
		for(int i=0;i<=size;i++) {
			System.out.println(H[i]);
		}
		
	}
	public void iterate() {
		
		for(int i=0;i<=size;i++) {
			System.out.println(H[i]);
		}
		
		
	}
	public static void Search(int key) {
		int index = 0;
		
		for(int i=0;i<size;i++) {
			if(H[i]!=key) {
				index++;
			
			}
		}
		if(index==0) {
			System.out.println("Element not found");
		}else {
			
			System.out.println("Element found at position="+index);
		}
	}
	public static void reverse() {
		System.out.println("Reverse Queue=");
		
		for(int i=size;i>=0;i--) {
			System.out.print(H[i] + " ");
		}
	}

	public void display() {
	
		System.out.print("Priority Queue : ");
		for(int j=0;j<=size;j++) {
			System.out.print(H[j] + " ");
		}
	
		System.out.print("\n");
	}

	public static int size() {
		int count=size;
		return count+1;
	
	}


}
