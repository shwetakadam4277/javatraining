package LinearDataStructure;

import java.util.Scanner;

public class StackCode {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		Mystacks m=new Mystacks();
		int q=1;
		System.out.println("Stack Working");
		
		while(q!=0) {
			
		System.out.println("1.Push  2. Pop   3.display   4.Reverse   5.Size   6.Peek    7.Contains/Search   8.Center   9.Sort   10.Iterate");
		int op=sc.nextInt();
		switch(op) {
		case 1:
			int ele=sc.nextInt();
			m.push(ele);
			break;
		case 2:
			int showele;
			showele=m.pop();
			System.out.println("Popped element is=");
			System.out.println(showele+" ");
			break;
		case 3:
			System.out.println("Elements in stack are=");
			m.display();
			break;
		case 4:
			System.out.println("Reversed elements are=");
			m.reverse();
			break;
		case 5:
			m.size();
			break;
		case 6:
			System.out.println("Peek element is=");
			m.peek();
			break;
		case 7:
//			System.out.println("Search no is already 30");
			System.out.println("Enter element to search=");
			int n=sc.nextInt();
			m.Search(n);
			break;
		case 8:
			m.Center();
			break;
		case 9:
			m.sort();
			break;
		case 10:
			m.iterate();
			break;
//		case 5:
//			q=0;
		}
		}
	}

}
